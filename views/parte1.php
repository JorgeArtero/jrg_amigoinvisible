<?php 
	if (isset($_GET['amigos'])) {
		$numeroAmigos=$_GET['amigos'];
		if ($_GET['amigos']<4) {
			echo '<b>No podemos crear un sorteo con menos de 4 personas</b><br>';
			$numeroAmigos=4;
		}
	}else{
		$numeroAmigos = 4;
	}
	
?>	
<a class="btn btn-primary btn-sm" href="index.php?p=parte1&amigos=<?php echo $numeroAmigos+1?>"><span class="glyphicon glyphicon-plus-sign">Añadir participante</span></a>

<?php
	if ($numeroAmigos>4){
		echo '<a class="btn btn-danger btn-sm" href="index.php?p=parte1&amigos='.($numeroAmigos-1).'"><span class="glyphicon glyphicon-minus-sign">Quitar participante</span></a>';	
	}
?>
<hr>
<form action="index.php?p=parte2" method="post">
<table class="table table-bordered table-condensed" border="1">
	<tr>
		<th>NOMBRE</th><th>CORREO</th>
	</tr>
<?php
	for ($i=1; $i<=$numeroAmigos; $i++) {	
		echo '<tr>';
			echo '<td><input type="text" name="nombre[]" placeholder="Nombre"></td>';
			echo '<td><input type="text" name="correo[]" placeholder="Correo electrónico"></td>';
		echo '</tr>';
	}
?>
</table>
<input type="hidden" name="numero" value="<?php echo $numeroAmigos; ?>">
<input type="submit" name="enviar" value="enviar">
</form>
