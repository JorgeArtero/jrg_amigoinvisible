<p>Enhorabuena. Tu sorteo  ha sido creado con éxito.</p>
<h4>Tu número de recuperación es <?php echo $idSorteo ?>.</h4>

<p>Este es el mensaje que enviaremos a todos los participantes.</p>
<div class="mensaje">
	<samp><p>Hola _____, sabemos que <?php echo $creador ?> te está obligando a participar en este amigo invisible, pero no te preocupes. Solo tienes que hacerle un regalo a ______ y serás libre. A continuación, te dejo un mensaje del organizador.</p>
	<p><em>"<?php echo $mensaje ?>"</em></p>
	<p>De parte de todo el equipo de AmigoInvisible, muchas gracias por participar.</p></samp>
</div>