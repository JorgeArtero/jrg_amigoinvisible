<?php  
//Incluyo mi modelo de amigo individual
include('models/sorteoModel.php');

//Modelo de amigos
Class sorteos extends MasterModel{
	public function __construct(){
		parent::__construct('sorteos');
	}

	public function insertarElemento($id){
		$sql="INSERT INTO $this->tabla (idSorteo, fecha, mensaje)VALUES('$id', NOW(), 'DEFAULT')";
		if($consulta=$this->conexion->query($sql)){
			return true;
		}else{
			return false;
		}
	}

	public function modificarElemento($id, $mensaje){
		$sql="UPDATE $this->tabla SET mensaje='$mensaje' WHERE idSorteo=$id";	
		if($consulta=$this->conexion->query($sql)){					
			return true;
		}else{			
			return false;
		}
	}

	public function recuperarElemento ($id){
		$sql="SELECT * FROM $this->tabla WHERE idSorteo=$id";
		$consulta=$this->conexion->query($sql);
		$fila=$consulta->fetch_array();
		$sorteo=new sorteo($fila['idSorteo'], $fila['fecha'], $fila['mensaje']);
		return $sorteo;
	}

	public function comprobarElemento($id){
		$sql="SELECT count(idSorteo) FROM $this->tabla WHERE idSorteo=$id";
		$consulta=$this->conexion->query($sql);
		$fila=$consulta->fetch_array();
		
		return $fila[0];
	}
}
?>