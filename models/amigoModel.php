<?php 

	class amigo{
		private $idAmigo;
		private $nombre;
		private $correo;	
		private $idSorteo;
		private $nombreAsignado;


		public function __construct($idAmigo, $nombre, $correo, $idSorteo, $nombreAsignado){
			$this->idAmigo = $idAmigo;
			$this->nombre = $nombre;
			$this->correo = $correo;
			$this->idSorteo = $idSorteo;
			$this->nombreAsignado = $nombreAsignado;
		}
		
		public function dimeId(){
		return $this->idAmigo;
		}
		public function dimeNombre(){
			return $this->nombre;
		}
		public function dimeCorreo(){
			return $this->correo;
		}
		public function dimeSorte(){
			return $this->idSorteo;
		}
		public function dimeAsignado(){
			return $this->nombreAsignado;
		}
	}
?>