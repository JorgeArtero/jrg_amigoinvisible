<?php 
include ('models/amigosModel.php');
include ('models/sorteosModel.php');
$sorteo = new sorteos();
$amigo = new amigos();

//Mensaje por defecto por si no se escribe uno.
$msg = "Anda que participar en el amigo invisible a mediados de mayo. Si es que...";

if (isset($_GET['p'])) {
	$p=$_GET['p'];
}else{
	$p='inicio';
}

switch ($p) {

	case 'inicio':
		include ('views/inicio.php');
		break;

	/********* Generador de sorteos *********/

	case 'parte1':
		include ('views/parte1.php');
		break;

	case 'parte2':
		$cont=0;
		//Recogemos los datos del formulario de la parte 1
		if (isset($_POST['enviar'])) {
			$numeroAmigos=$_POST['numero'];
			foreach ($_POST["nombre"] as $n) {
				$nombre[$cont]=$n;		
				$cont++;
			}
			$cont=0;
			foreach ($_POST["correo"] as $c) {
				$correo[$cont]=$c;
				$cont++;
			}
			
			//Generamos el num de recuperación del sorteo y lo insertamos en la BD
			$idSorteo = numero_sorteo($sorteo);			
			$sorteo -> insertarElemento($idSorteo);	

			//Realizamos el sorteo
			$asignado=asignar_amigo($nombre);

			include ('views/parte2.php');
		}
		break;

	case 'parte3':		
		if (isset($_POST['enviar'])) {
			//Recogemos los datos del formulario de la parte 2
			$idSorteo = $_POST['idSorteo'];
			$creador = $_POST['creador'];
			if (empty($_POST['mensaje']) === true) {
				$mensaje = $msg;
			}else{
				$mensaje = $_POST['mensaje'];
			}	

			$amigos = $amigo -> recuperarElemento($idSorteo);

			if ($sorteo -> modificarElemento($idSorteo, $mensaje) === true){
				include ('views/parte3.php');

				//Generamos el mail y lo enviamos
				include ('includes/enviar.php');

			}else{
				echo "<p>Ha ocurrido un error. Por favor, inténtenlo de nuevo más tarde.</p>";
				echo "<p><a href='index.php'>Volver al inicio</a></p>";		
				//Aquí borrariamos el sorteo creado de la BD.
			}		
		}
		break;

	/********* Recuperador de sorteos *********/
	
	case 'recuperar':
		include 'views/recuperarSorteo.php';
		break;

	case 'mostrar':
		if (isset($_POST['enviar'])) {
			//Recogemos los datos del formulario
			$idSorteo = $_POST['idSorteo'];
			
			if ($idSorteo != null) {					
				$amigos = $amigo -> recuperarElemento($idSorteo);
				include 'views/mostrar.php';
			}else{
				echo "Necesitas indicar un sorteo para poder recuperarlo.";
			}

		}else{
			echo "Necesitas indicar un sorteo para poder recuperarlo.";
		}
		break;	

	case 'reenviar':
		if (isset($_POST['enviar'])) {
			//Recogemos los datos del formulario anterior
			$idSorteo = $_POST['idSorteo'];
			$nombre = $_POST['nombre'];
			$correo = $_POST['correo'];
			$id = $_POST['id'];
			$asignado = $_POST['asignado'];
			include 'includes/recuperar.php';
			include 'views/reenviar.php';
		}
		break;
	
	default:
		break;
}
?>